<?xml version="1.0" encoding="UTF-8"?>
<!--
  ! CCPL HEADER START
  !
  ! This work is licensed under the Creative Commons
  ! Attribution-NonCommercial-NoDerivs 3.0 Unported License.
  ! To view a copy of this license, visit
  ! http://creativecommons.org/licenses/by-nc-nd/3.0/
  ! or send a letter to Creative Commons, 444 Castro Street,
  ! Suite 900, Mountain View, California, 94041, USA.
  !
  ! You can also obtain a copy of the license at
  ! trunk/opendj3/legal-notices/CC-BY-NC-ND.txt.
  ! See the License for the specific language governing permissions
  ! and limitations under the License.
  !
  ! If applicable, add the following below this CCPL HEADER, with the fields
  ! enclosed by brackets "[]" replaced with your own identifying information:
  !      Portions Copyright [yyyy] [name of copyright owner]
  !
  ! CCPL HEADER END
  !
  !      Copyright 2014 ForgeRock AS
  !    
-->
<refentry xml:id='addrate-1'
          xmlns='http://docbook.org/ns/docbook'
          version='5.0' xml:lang='en'
          xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
          xsi:schemaLocation='http://docbook.org/ns/docbook
                              http://docbook.org/xml/5.0/xsd/docbook.xsd'
          xmlns:xlink='http://www.w3.org/1999/xlink'
          xmlns:xinclude='http://www.w3.org/2001/XInclude'>
 <info>
  <copyright>
   <year>2014</year>
   <holder>ForgeRock AS</holder>
  </copyright>
 </info>

 <refmeta>
  <refentrytitle>addrate</refentrytitle><manvolnum>1</manvolnum>
  <refmiscinfo class="software">OpenDJ</refmiscinfo>
  <refmiscinfo class="version">${docTargetVersion}</refmiscinfo>
 </refmeta>

 <refnamediv>
  <refname>addrate</refname>
  <refpurpose>measure add &amp; delete throughput and response time</refpurpose>
 </refnamediv>

 <refsynopsisdiv>
  <cmdsynopsis>
   <command>addrate</command>
   <arg choice="req">options</arg>
   <arg choice="req">template-file-path</arg>
  </cmdsynopsis>
 </refsynopsisdiv>

 <refsect1>
  <title>Description</title>
  <para>
   This utility can be used to measure add and optionally delete
   throughput and response time of a directory server using user-defined entries.
  </para>

  <para>
   The <replaceable>template-file-path</replaceable> argument
   identifies a template file that has the same form as a template file
   for the <command>makeldif</command> command.
   For details, see
   <link
    xlink:show="new"
    xlink:href="dev-guide#makeldif-template-5"
    xlink:role="http://docbook.org/xlink/role/olink"
   >makeldif.template</link>.
  </para>

  <xinclude:include href="../shared/informalexample-net-tweaks.xml" />
 </refsect1>

 <refsect1>
  <title>Options</title>

  <para>
   The following options are supported.
  </para>

  <variablelist>
   <varlistentry>
    <term><option>-a, --deleteAgeThreshold {seconds}</option></term>
    <listitem>
     <para>
      Specifies the age at which added entries will become candidates for deletion
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><option>-A, --asynchronous</option></term>
    <listitem>
     <para>
      Use asynchronous mode and do not wait for results before sending the next request
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><option>-B, --warmUpDuration {warmUpDuration}</option></term>
    <listitem>
     <para>
      Warm up duration in seconds
     </para>

     <para>
      Default value: 0
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><option>-c, --numConnections {numConnections}</option></term>
    <listitem>
     <para>
      Number of connections
     </para>

     <para>
      Default value: 1
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><option>-C, --deleteMode {fifo | random | off}</option></term>
    <listitem>
     <para>
      The algorithm used for selecting entries to be deleted which must be one of "fifo", "random", or "off".
     </para>

     <para>
      Default value: FIFO
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><option>-d, --maxDuration {maxDuration}</option></term>
    <listitem>
     <para>
      Maximum duration in seconds, 0 for unlimited
     </para>

     <para>
      Default value: 0
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><option>-e, --percentile {percentile}</option></term>
    <listitem>
     <para>
      Calculate max response time for a percentile of operations
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><option>-f, --keepConnectionsOpen</option></term>
    <listitem>
     <para>
      Keep connections open
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><option>-F, --noRebind</option></term>
    <listitem>
     <para>
      Keep connections open and do not rebind
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><option>-g, --constant {name=value}</option></term>
    <listitem>
     <para>
      A constant that overrides the value set in the template file
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><option>-i, --statInterval {statInterval}</option></term>
    <listitem>
     <para>
      Display results each specified number of seconds
     </para>

     <para>
      Default value: 5
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><option>-m, --maxIterations {maxIterations}</option></term>
    <listitem>
     <para>
      Max iterations, 0 for unlimited
     </para>

     <para>
      Default value: 0
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><option>-M, --targetThroughput {targetThroughput}</option></term>
    <listitem>
     <para>
      Target average throughput to achieve
     </para>

     <para>
      Default value: 0
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><option>-r, --resourcePath {path}</option></term>
    <listitem>
     <para>
      Path to look for template resources (e.g. data files)
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><option>-R, --randomSeed {seed}</option></term>
    <listitem>
     <para>
      The seed to use for initializing the random number generator
     </para>

     <para>
      Default value: 0
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><option>-s, --deleteSizeThreshold {count}</option></term>
    <listitem>
     <para>
      Specifies the number of entries to be added before deletion begins
     </para>

     <para>
      Default value: 10000
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><option>-S, --scriptFriendly</option></term>
    <listitem>
     <para>
      Use script-friendly mode
     </para>
    </listitem>
   </varlistentry>

   <varlistentry>
    <term><option>-t, --numThreads {numThreads}</option></term>
    <listitem>
     <para>
      Number of worker threads per connection
     </para>

     <para>
      Default value: 1
     </para>
    </listitem>
   </varlistentry>   
  </variablelist>

  <refsect2>
   <title>LDAP Connection Options</title>

   <variablelist>
    <varlistentry>
     <term><option>-D, --bindDN {bindDN}</option></term>
     <listitem>
      <para>
       DN to use to bind to the server
      </para>

      <para>
       If you do not specify a bind DN,
       then the operations are performed as an anonymous user.
       In many directories anonymous users have no right to add or delete entries.
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>-E, --reportAuthzID</option></term>
     <listitem>
      <para>
       Use the authorization identity control
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>-h, --hostname {host}</option></term>
     <listitem>
      <para>
       Directory server hostname or IP address
      </para>

      <para>
       Default value: localhost.localdomain
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>-j, --bindPasswordFile {bindPasswordFile}</option></term>
     <listitem>
      <para>
       Bind password file
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>-K, --keyStorePath {keyStorePath}</option></term>
     <listitem>
      <para>
       Certificate key store path
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>-N, --certNickname {nickname}</option></term>
     <listitem>
      <para>
       Nickname of the certificate that the server should use
       when accepting SSL-based connections or performing StartTLS negotiation
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>-o, --saslOption {name=value}</option></term>
     <listitem>
      <para>
       SASL bind options
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>-p, --port {port}</option></term>
     <listitem>
      <para>
       Directory server port number
      </para>

      <para>
       Default value: 389
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>-P, --trustStorePath {trustStorePath}</option></term>
     <listitem>
      <para>
       Certificate trust store path
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>-q, --useStartTLS</option></term>
     <listitem>
      <para>
       Use StartTLS to secure communication with the server
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>-T, --trustStorePassword {trustStorePassword}</option></term>
     <listitem>
      <para>
       Certificate trust store PIN
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>-u, --keyStorePasswordFile {keyStorePasswordFile}</option></term>
     <listitem>
      <para>
       Certificate key store PIN file.
       A PIN is required when you specify to use an existing certificate
       as server certificate
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>-U, --trustStorePasswordFile {path}</option></term>
     <listitem>
      <para>
       Certificate trust store PIN file
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>--usePasswordPolicyControl</option></term>
     <listitem>
      <para>
       Use the password policy request control
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>-w, --bindPassword {bindPassword}</option></term>
     <listitem>
      <para>
       Password to use to bind to the server
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>-W, --keyStorePassword {keyStorePassword}</option></term>
     <listitem>
      <para>
       Certificate key store PIN
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>-X, --trustAll</option></term>
     <listitem>
      <para>
       Trust all server SSL certificates
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>-Z, --useSSL</option></term>
     <listitem>
      <para>
       Use SSL for secure communication with the server
      </para>
     </listitem>
    </varlistentry>
   </variablelist>
  </refsect2>

  <refsect2>
   <title>Utility Input/Output Options</title>

   <variablelist>
    <varlistentry>
     <term><option>--noPropertiesFile</option></term>
     <listitem>
      <para>
       No properties file will be used to get default command line argument values
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>--propertiesFilePath {propertiesFilePath}</option></term>
     <listitem>
      <para>
       Path to the file containing default property values used for command line arguments
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term><option>-v, --verbose</option></term>
     <listitem>
      <para>
       Use verbose mode
      </para>
     </listitem>
    </varlistentry>
   </variablelist>
  </refsect2>

  <refsect2>
   <title>General Options</title>

   <variablelist>
    <varlistentry>
     <term><option>-V, --version</option></term>
     <listitem>
      <para>
       Display version information
      </para>
     </listitem>
    </varlistentry>

     <varlistentry>
     <term><option>-?, -H, --help</option></term>
     <listitem>
      <para>
       Display usage information
      </para>
     </listitem>
    </varlistentry>
   </variablelist>
  </refsect2>
 </refsect1>

 <refsect1>
  <title>Exit Codes</title>

   <variablelist>
    <varlistentry>
     <term>0</term>
     <listitem>
      <para>
       The command completed successfully.
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term>80</term>
     <listitem>
      <para>
       The command could not complete due to an input/output error.
      </para>
     </listitem>
    </varlistentry>

    <varlistentry>
     <term>89</term>
     <listitem>
      <para>
       An error occurred while parsing the command-line arguments.
      </para>
     </listitem>
    </varlistentry>
   </variablelist>
 </refsect1>

 <refsect1>
  <title>Examples</title>

  <para>
   The following examples use this template file,
   <filename>addrate.template</filename>.
  </para>

  <programlisting language="ldif">
<![CDATA[
define suffix=dc=example,dc=com
define maildomain=example.com

branch: [suffix]

branch: ou=People,[suffix]
subordinateTemplate: person

template: person
rdnAttr: uid
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
givenName: <first>
sn: <last>
cn: {givenName} {sn}
initials: {givenName:1}<random:chars:ABCDEFGHIJKLMNOPQRSTUVWXYZ:1>{sn:1}
employeeNumber: <sequential:0>
uid: user.{employeeNumber}
mail: {uid}@[maildomain]
userPassword: password
telephoneNumber: <random:telephone>
homePhone: <random:telephone>
pager: <random:telephone>
mobile: <random:telephone>
street: <random:numeric:5> <file:streets> Street
l: <file:cities>
st: <file:states>
postalCode: <random:numeric:5>
postalAddress: {cn}${street}${l}, {st}  {postalCode}
description: This is the description for {cn}.

]]>
  </programlisting>

  <para>
   The following example adds entries, and then randomly deletes them
   when more than 10,000 entries have been added.
  </para>

  <screen>
$ <userinput>addrate -p 1389 -D "cn=Directory Manager" -w password \
 -f -c 10 -C random -s 10000 addrate.template</userinput>
<computeroutput>
-----------------------------------------------------------------------
     Throughput                            Response Time
   (ops/second)                           (milliseconds)
recent  average  recent  average  99.9%  99.99%  99.999%  err/sec  Add%
-----------------------------------------------------------------------
 400.0    401.3  24.304   24.304  205.497  208.726  208.726      0.0  100.00
 254.9    327.9  38.836   29.970  216.574  291.131  291.131      0.0  100.00
 576.8    411.0  17.176   23.977  208.726  291.131  291.131      0.0  100.00
 876.0    527.3  11.119   18.633  195.236  255.678  291.131      0.0  93.93
 758.0    573.5  12.959   17.133  189.765  255.678  291.131      0.0  49.97
^C</computeroutput>
  </screen>

  <para>
   The following example also adds entries, and then deletes them
   in the order they were added after they are 10 seconds old.
  </para>

  <screen>
$ <userinput>addrate -p 1389 -D "cn=Directory Manager" -w password \
 -f -c 10 -C fifo -a 10 addrate.template</userinput>
<computeroutput>-----------------------------------------------------------------------
     Throughput                            Response Time
   (ops/second)                           (milliseconds)
recent  average  recent  average  99.9%  99.99%  99.999%  err/sec  Add%
-----------------------------------------------------------------------
2656.8   2665.4   3.478    3.478  66.896  170.495  185.655   2006.4  100.00
 643.2   1650.1  15.342    5.799  74.356  170.495  185.655      0.0  99.41
 830.2   1376.3  11.778    7.004  88.155  271.071  284.580      0.0  0.02
 901.1   1257.3  10.843    7.692  81.984  271.071  284.580      0.0  49.41
 597.4   1125.3  16.542    8.633  89.126  271.071  284.580      0.0  100.00
^C</computeroutput>
  </screen>
 </refsect1>
</refentry>
